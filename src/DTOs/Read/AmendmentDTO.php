<?php declare(strict_types=1);

/**
 * This file is part of the Zuora PHP API Client, a PHP Experts, Inc., Project.
 *
 * Copyright © 2019 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *  GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *  https://www.phpexperts.pro/
 *  https://github.com/phpexpertsinc/Zuora-API-Client
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\ZuoraClient\DTOs\Read;

use Carbon\Carbon;
use PHPExperts\SimpleDTO\SimpleDTO;

/**
 * See https://www.zuora.com/developer/API-Reference/#operation/GET_AmendmentsByKey
 *
 * @property bool        $success
 * @property string      $id
 * @property bool        $autoRenew
 * @property null|string $baseRatePlanId
 * @property null|string $baseSubscriptionId
 * @property string      $code
 * @property Carbon      $contractEffectiveDate
 * @property int         $currentTerm
 * @property string      $currentTermPeriodType
 * @property Carbon      $customerAcceptanceDate
 * @property string      $description
 * @property string      $destinationAccountId
 * @property string      $destinationInvoiceOwnerId
 * @property Carbon      $effectiveDate
 * @property string      $name
 * @property null|string $newRatePlanId
 * @property string      $newSubscriptionId
 * @property string      $renewalSetting
 * @property int         $renewalTerm
 * @property string      $renewalTermPeriodType
 * @property Carbon      $resumeDate
 * @property Carbon      $serviceActivationDate
 * @property Carbon      $specificUpdateDate
 * @property string      $status
 * @property Carbon      $suspendDate
 * @property Carbon      $termStartDate
 * @property string      $termType
 * @property string      $type
 */
class AmendmentDTO extends SimpleDTO
{
}
